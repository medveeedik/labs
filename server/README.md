# README #

Simple tcp client-server which stores data in PostgreSQL

### Installation ###

Install virtualbox and vagrant.

Then make:

	git clone https://vvoronova@bitbucket.org/vvoronova/systememulator.git foldername

	cd foldername

	vagrant up

### Usage ###

To start server:

	vagrant ssh

	python3 /vagrant/Task1/myserver.py &

To start client:

	python3 /vagrant/Task1/myclient.py unique_number

where unique_number is 10-sumbol indetifier of current device.

Local command to access the database via psql:
	
	PGUSER=myserver_db PGPASSWORD=myserver_db psql -h localhost -p 15432 myserver_db
