from db_preparation import preparation
import psycopg2
import datetime


class DBConnector(object):

    def __init__(self, db_name, db_user, db_password,
                       db_host, table_user, table_files):
        self.table_user = table_user
        self.table_files = table_files
        self.db_name = db_name
        self.db_user = db_user
        self.db_password = db_password
        self.db_host = db_host

        self.connection = None
        self.cursor = None
        self.status = None

    def __enter__(self):
        self.connection, self.status = preparation(self.db_name, self.db_user, self.db_password,
                                      self.db_host, self.table_user, self.table_files)
        if self.connection:
            self.cursor = self.connection.cursor()
        return self

    def view_all_users(self):
        self.cursor.execute("SELECT * FROM " % self.table_user)
        return self.cursor.fetchall()

    def view_all_passed_data(self):
        self.cursor.execute("SELECT * FROM" % self.table_files)
        return self.cursor.fetchall()

    def insert_recieved_data(self, current_user_id, current_data, recieved_at):
        self.cursor.execute('INSERT INTO recieved_files (id, data, recieved_at) VALUES (%s, %s, %s)',
                             (current_user_id, current_data, recieved_at))
        self.connection.commit()

    def create_user_or_update_status(self, new_user_id, new_user_status):
        try:
            self.cursor.execute("INSERT INTO users (id, status) VALUES(%s, %s);", (new_user_id, new_user_status))
        except psycopg2.Error:
            self.connection.commit()
            self.cursor.execute("UPDATE users SET status=%s WHERE id=%s ", (new_user_status, new_user_id))
            self.connection.commit()

    def __exit__(self, exc_type, exc_val, exc_tb):

        if self.connection:
            self.connection.close()

        if self.status:
            return True
        elif exc_type is AttributeError:
            self.status = 'attribute error'
        elif exc_type:
            self.status = 'database connetion error'

        return True

