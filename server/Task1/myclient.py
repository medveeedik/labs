from myserver import config
import datetime
import socket
import json
import time
import sys


def client_job(number):
    received = None

    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    try:
        BODY_REQUEST = {
            'some_data': str(datetime.datetime.now())
        }

        current_device_number = bytes(number, 'utf-8') if len(number) == config['LENGTH'] \
            else bytes(config['NUMBER'], 'utf-8')

        sock.connect((config['HOST'], config['PORT']))
        sock.sendall(current_device_number)
        sock.sendall(bytes(json.dumps(BODY_REQUEST), "utf-8"))
        received = str(sock.recv(2 + config['LENGTH']), "utf-8")
        print(received)
    finally:
        sock.close()
        return received


def client_cycle(number, delay):
    while True:
        try:
            status = client_job(number)
            time.sleep(delay)
        except KeyboardInterrupt:
            break


if __name__=='__main__':
    number = sys.argv[1]
    delay = 5
    client_cycle(number, delay)