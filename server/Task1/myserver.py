from configparser import ConfigParser
import socketserver
import logging
import os
from db_execution import DBConnector
import sys
import datetime
import psycopg2


f = "%Y-%m-%d %H:%M:%S.%f"


def read_config(path):
    try:
        config = ConfigParser()
        config.read(path)
        params = {
            'SIZE': config.getint('Chunk', 'size'),
            'PORT': config.getint('Server', 'port'),
            'HOST': config.get('Server', 'host'),
            'N': config.getint('Server', 'n'),
            'DBNAME': config.get('Database', 'dbname'),
            'USER': config.get('Database', 'user'),
            'PASSWORD': config.get('Database', 'password'),
            'USER_TABLE': config.get('Database', 'user_table'),
            'FILES_TABLE': config.get('Database', 'files_table'),
            'LENGTH': config.getint('Request', 'length'),
            'NUMBER': config.get('Request', 'number'),
        }
    except:
        return
    return params


config = read_config(os.path.join(os.path.abspath(os.path.dirname(__file__)), 'config'))
if not config:
    sys.exit('Cant get params for server')


def write_package(package, user_id):
    with open('notsended/' + user_id + str(datetime.datetime.now()), 'w') as fin:
        fin.write(str(package))


class MyTCPHandler(socketserver.BaseRequestHandler):

    def setup(self):
        self.db_connector = self.server.db_connector
        if not self.db_connector:
            sys.exit('Database connection failed')
        self.dir = os.path.abspath(os.path.dirname(__file__))
        logging.basicConfig(filename=os.path.join(os.path.abspath(os.path.dirname(__file__)), 'sample.log'), level=logging.INFO)
        self.status = None
        self.data = b''
        if os.path.exists(os.path.join(self.dir, 'notsended')):
            for path in os.listdir(os.path.join(self.dir, 'notsended')):
                with open(os.path.join(self.dir, 'notsended', path), 'r') as fout:
                    data = fout.read()
                    self.db_connector.insert_recieved_data(path[:config['LENGTH']], data, datetime.datetime.strptime(path[10:], f))
                    self.db_connector.create_user_or_update_status(path[:config['LENGTH']], 'off')
                    logging.info('from backup '+ str(path[:config['LENGTH']]) + str(data) + str(datetime.datetime.strptime(path[10:], f)))
                os.remove(os.path.join(self.dir, 'notsended', path))
                logging.info('removed '+os.path.join(self.dir, 'notsended', path) )

    def handle(self):

        self.user = self.request.recv(config['LENGTH'])

        logging.info(self.user + b' started')

        self.db_connector.create_user_or_update_status(self.user, 'on')
        while True:
            self.chunk = self.request.recv(config['SIZE'])
            self.data += self.chunk
            if len(self.chunk) < config['SIZE']:
                break
        try:
            self.db_connector.insert_recieved_data(self.user, self.data, str(datetime.datetime.now()))
            self.db_connector.create_user_or_update_status(self.user, 'off')
            self.request.sendall(b"OK" + self.user)

        except psycopg2.OperationalError:
            write_package(self.data, self.user)
            self.request.sendall(b"OK" + self.user)

        self.status = self.db_connector.status

        if self.status:
            logging.info(self.user + b' ' + bytes(self.status, 'utf-8'))
        logging.info(self.user + b' ' + self.data)
        logging.info(self.user + b' finished')

        return self.status


if __name__=='__main__':

    with DBConnector(db_name=config['DBNAME'],db_user=config['USER'],db_host=config['HOST'],
                     db_password=config['PASSWORD'], table_user=config['USER_TABLE'], table_files=config['FILES_TABLE']) as db_connector:
        server = socketserver.TCPServer((config['HOST'], config['PORT']), MyTCPHandler)
        server.db_connector = db_connector
        server.serve_forever()

