import psycopg2


def preparation(db_name, db_user, db_password,
                db_host, table_user, table_files):
    try:
        connection = psycopg2.connect(
            'host=%s dbname=%s user=%s password=%s' % (db_host, db_name, db_user, db_password))
        cursor = connection.cursor()
        cursor.execute("CREATE TABLE IF NOT EXISTS " + table_user + " (id bytea PRIMARY KEY, status VARCHAR(3));")
        cursor.execute(
            "CREATE TABLE IF NOT EXISTS " + table_files + " (id bytea, data bytea, recieved_at TIMESTAMP );")
        connection.commit()
        return connection, None
    except psycopg2.OperationalError:
        return None, "failure in name resolution"
    except:
        return None, 'database connection failure'

if __name__ == '__main__':
    print (preparation('localhost', 'myserverdatabase', 'lollol', 'lollol', 'usr', 'lol'))
